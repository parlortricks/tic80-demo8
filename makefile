# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (c) 2021 parlortricks
include config.mk

compile: out.lua
	ls $(SRC_DIR)*lua | entr make out.lua & echo "$$!" > "compile.pid"

run: out.lua
	tic80 --skip --fs . --cmd "load $(CART).tic & import code out.lua & run"

out.lua: $(SRC) ; cat $^ > $@

count: out.lua ; cloc $^

export-zip: out.lua
	mkdir -p build
	tic80 --cli --fs $(CURDIR) --cmd "load $(CART).tic & import code out.lua & export html $(CART) & exit"
	mv $(CURDIR)/$(CART).zip $(CURDIR)/build/$(CART).zip

export-cart: out.lua
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.lua & save out.tic & exit"
	mv $(CURDIR)/out.tic $(CURDIR)/build/$(CART).tic

export-png: out.lua
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.lua & save out.png & exit"
	mv $(CURDIR)/out.png $(CURDIR)/build/$(CART).png

export-win: out.lua
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.lua & export win out.exe alone=1 & exit"
	mv $(CURDIR)/out.exe $(CURDIR)/build/$(CART).exe

export-linux: out.lua
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.lua & export linux out alone=1 & exit"
	mv $(CURDIR)/out $(CURDIR)/build/$(CART)

export-mac: out.lua
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.lua & export mac out.app alone=1 & exit"
	mv $(CURDIR)/out.app $(CURDIR)/build/$(CART).app	

upload: $(CURDIR)/build/$(CART).zip
	butler push $(CURDIR)/build/$(CART).zip $(ITCH_USER)/$(PROJECT):$(CART)

status: 
	butler status $(ITCH_USER)/$(PROJECT):$(CART)

todo: ; grep -in TODO $(SRC)
fixme: ; grep -in FIXME $(SRC)

clean: 
	rm -rf build
	rm -rf out.lua
	rm -rf .local
	cat compile.pid | xargs kill -9
	rm -rf compile.pid
